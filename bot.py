import telebot
import config
import random
import pars, pars_silpo

from telebot import types

bot = telebot.TeleBot(config.TOKEN)


@bot.message_handler(commands=['start'])
def welcome(message):
    # sti = open('c:\luke.jpg', 'rb')
    # bot.send_sticker(message.chat.id, sti)

    # keyboard
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    item1 = types.KeyboardButton("🎲 Рандомное число")
    item2 = types.KeyboardButton("😊 Чиї ціни показати?")

    markup.add(item1, item2)

    bot.send_message(message.chat.id,
                     "Привіт, {0.first_name}!\nЯ - <b>{1.first_name}</b>, бот створений, щоб показувати актуальні ціни у онлайн магазині АТБ, Рукавичка, Сільпо.".format(
                         message.from_user, bot.get_me()),
                     parse_mode='html', reply_markup=markup)


@bot.message_handler(content_types=['text'])
def lalala(message):
    if message.chat.type == 'private':
        if message.text == '🎲 Рандомное число':
            bot.send_message(message.chat.id, str(random.randint(0, 100)))
        elif message.text == '😊 Чиї ціни показати?':

            markup = types.InlineKeyboardMarkup(row_width=3)
            item1 = types.InlineKeyboardButton("АТБ", callback_data='ATB')
            item2 = types.InlineKeyboardButton("Львівхолод", callback_data='Lvivholod')
            item3 = types.InlineKeyboardButton("Сільпо", callback_data='silpo')

            markup.add(item1, item2, item3)

            bot.send_message(message.chat.id, ' Чиї ціни показати?', reply_markup=markup)
        else:
            bot.send_message(message.chat.id, 'Я не знаю что ответить 😢')


@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    try:
        if call.message:
            if call.data == 'ATB':
                temp = pars.pars_a()
                for item in temp:
                    # mess = f"<a href='{item.get('link_product')}'><b>{item.get('title')}</b></a>  <i>price</i> {item.get('price_sale')}"
                    mess = f"{item.get('title')}  <i>price</i> {item.get('price_sale')}"
                    bot.send_message(call.message.chat.id, mess, parse_mode='html')
                bot.send_message(call.message.chat.id, "ATB")
            elif call.data == 'Lvivholod':
                temp = pars.pars_r()
                for item in temp:
                    # mess = f"<a href='{item.get('link_product')}'>{item.get('title')}</a>  <i>price</i> {item.get('price_sale')}"
                    mess = f"{item.get('title')}  <i>price</i> {item.get('price_sale')}"
                    bot.send_message(call.message.chat.id, mess, parse_mode='html')
                bot.send_message(call.message.chat.id, "Lvivholod")
            elif call.data == 'silpo':
                temp = pars_silpo.pars_s()
                for item in temp:
                    # mess = f"<a href='{item.get('link_product')}'>{item.get('title')}</a>  <i>price</i> {item.get('price_sale')}"
                    mess = f"{item.get('title')}  <i>price</i> {item.get('price_sale')}"
                    bot.send_message(call.message.chat.id, mess, parse_mode='html')
                bot.send_message(call.message.chat.id, "Silpo")

            # remove inline buttons
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="😊 Чиї ціни показати?",
                                  reply_markup=None)

            # show alert
            bot.answer_callback_query(callback_query_id=call.id, show_alert=False,
                                      text="Пошук цін завершено")

    except Exception as e:
        print(repr(e))


# RUN
bot.polling(none_stop=True)