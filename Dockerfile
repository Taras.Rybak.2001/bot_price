FROM python:3.10-alpine

ENV PYTHONUNBUFFERED 1

COPY ./bot.py /bot.py
COPY ./config.py /config.py
COPY ./pars.py /pars.py
COPY ./pars_silpo.py /pars_silpo.py
COPY ./requirements.txt /requirements.txt
RUN pip install -r /requirements.txt


RUN adduser -D user

USER user

CMD ["python", "bot.py"]