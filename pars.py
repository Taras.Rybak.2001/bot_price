#pip3 install pytelegrambotapi --upgrade
import requests
from bs4 import BeautifulSoup
import csv

HOST = "https://zakaz.atbmarket.com"
URL = "https://zakaz.atbmarket.com/catalog/439-siri-plavleni"

URL_RYK = "https://market.rukavychka.ua/sir/?sort=p.price&order=ASC&limit=100"
HOST_R = "https://market.rukavychka.ua"

URL_S = "https://shop.silpo.ua/category/syry-1468?sort_side=price-desc&filter_SUB=(1471)&to=5&from=1"
HOST_S = "https://shop.silpo.ua"

HEADERS_atb = {
    "Accept" : "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
    "User-Agent" : "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36",
    "Cookie" : "newuser=1662308893; _session=g0kv9uls7dt3pdk2j6ng8i3djn; _csrf-shop=6376bb3946d166a344117083fe87102c9a9b99ceef15ab134a410c7a2ca03f33a%3A2%3A%7Bi%3A0%3Bs%3A10%3A%22_csrf-shop%22%3Bi%3A1%3Bs%3A32%3A%22mogCNa5dI3gO31TyyZ_bF-DPew6VwGim%22%3B%7D; _ms=0fb5dd31-d703-43e4-ad45-4ca2ebd648f8; sc=075F6147-E370-D35F-39A6-4C3822645D32; _hjSessionUser_1764046=eyJpZCI6IjExY2E0MjQyLTNkNzAtNTRkZC1iYWM1LTc0MzhiMzJmZjVhMiIsImNyZWF0ZWQiOjE2NjIzMDg5MjYzMDksImV4aXN0aW5nIjp0cnVlfQ==; viewedprod=a%3A1%3A%7Bi%3A0%3Bs%3A6%3A%22163462%22%3B%7D; atbdelivery=0; atbaddressregion=61; packs=3; store_id=983; _gid=GA1.2.895411162.1662750845; _hjSession_1764046=eyJpZCI6IjMyNDI3MGE0LTA5ODMtNDFmMS1iYjM4LTM3NmRhMWY3MjcxNSIsImNyZWF0ZWQiOjE2NjI3NTA4NDU3MDYsImluU2FtcGxlIjpmYWxzZX0=; _hjIncludedInSessionSample=0; banner=545%2C549%2C559%2C533; _ga_6MXFKBMVX6=GS1.1.1662750844.2.1.1662751847.37.0.0; _ga=GA1.2.572883005.1662308926",
}

HEADERS_ryk = {
    "Accept" : "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
    "User-Agent" : "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36",
    "Cookie" : "OCSESSID=0f2e0fde8782db601f3072f36f; language=uk-ua; currency=UAH; cart_session_id=0f2e0fde8782db601f3072f36f; _ga=GA1.2.2145940541.1662758143; _gid=GA1.2.1993152816.1662758143; _hjFirstSeen=1; _hjSession_2607241=eyJpZCI6ImFhMzlhYmU2LWI1ODgtNDM1YS04OTBkLWJjN2E2ZmFkZTZlZCIsImNyZWF0ZWQiOjE2NjI3NTgxNDMyMzMsImluU2FtcGxlIjpmYWxzZX0=; _hjIncludedInSessionSample=0; mr_policy=1; _hjSessionUser_2607241=eyJpZCI6IjZjMGYyYTNiLTlkNDAtNTM3NC1iMjZkLTc3ZDhmYTkzMjY4YyIsImNyZWF0ZWQiOjE2NjI3NTgxNDMxMjMsImV4aXN0aW5nIjp0cnVlfQ==",
}

HEADERS_sil = {
    "Accept" : "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
    "User-Agent" : "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36",
    "Cookie" : "BIGipServer~ext-web-sites~POOL_WAF_shop.silpo.ua_80_NEW=rd4o00000000000000000000ffffc0a85e68o81; fingerprint=340a6ea8f5f01fe252cfcb659a0f30b2; filial_id=2043; _gid=GA1.2.525145906.1662766569; _clck=1egjzzm|1|f4q|0; _ga=GA1.2.1817999014.1662766569; agreeUseCookies=1662766628779; TS84299e33027=081080eb62ab20009a1d572c4d16317e0e90d171f4b42e23023c06bdd5927bf25261815f62296d890875697e3b1130008b317b79e15e7a42ff18b3d79fe7a14e244e7a9b2d414e9e8f9d3908d699ec3c18169239190a1568239f0c080ed78a3e; _ga_84V0NWZF8N=GS1.1.1662798340.2.0.1662798345.55.0.0; _ga_QCMSL3JRRH=GS1.1.1662798340.2.0.1662798345.55.0.0"
}


def get_html(url, head, params=""):
    r = requests.get(url, headers=head, verify=False)
    return r


def get_content_atb(html):
    soup = BeautifulSoup(html.text, "html.parser")
    items = soup.find_all("article", class_="catalog-item js-product-container")
    cards = []
    # print(items)
    for item in items:
        cards.append(
            {
                "title": item.find('div', class_="catalog-item__title").get_text().strip(),
                "link_product": HOST + item.find('div', class_="catalog-item__title").find("a").get("href"),
                "price_sale": item.find('data', class_="product-price__top").get("value"),
                # "price": item.find('data', class_="product-price__bottom").get("value"),

            }
        )
    return cards


def get_content_r(html):
    soup = BeautifulSoup(html.text, "html.parser")
    items = soup.find_all("div", class_="product-layout product-grid col-6 col-md-4 col-lg-4")
    cards = []
    # print(items)
    for item in items:
        cards.append(
            {
                "title": item.find('div', class_="fm-module-title").get_text().strip(),
                "link_product": item.find('div', class_="fm-module-title").find("a").get("href"),
                "price_sale": item.find('span', class_="fm-module-price-new").get_text(),
                # "price": item.find('data', class_="product-price__bottom").get("value"),

            }
        )
    return cards


def get_content_s(html):
    soup = BeautifulSoup(html.text, "html.parser")
    items = soup.find_all("div", class_="product-list-item")
    cards = []
    print(items)
    for item in items:
        cards.append(
            {
                "title": item.find('div', class_="catalog-item__title").get_text().strip(),
                "link_product": HOST + item.find('div', class_="catalog-item__title").find("a").get("href"),
                "price_sale": item.find('data', class_="product-price__top").get("value"),
                # "price": item.find('data', class_="product-price__bottom").get("value"),

            }
        )
    return cards


def pars_a():
    htmlv = get_html(URL, head=HEADERS_atb)
    rez = get_content_atb(htmlv)
    return rez


def pars_r():
    htmlv2 = get_html(URL_RYK, head=HEADERS_ryk)
    rez = get_content_r(htmlv2)
    return rez


def pars_s():
    htmlv2 = get_html(URL_S, head=HEADERS_sil)
    print(htmlv2)
    rez = get_content_s(htmlv2)
    return rez
# print(rez)

